from typing import Any, Protocol

import redis


class Storage(Protocol):
    def get_value(self, key: str) -> Any | None:
        """get() retrieves the value for a key"""

    def set_value(self, key: str, value: Any) -> None:
        """set() stores a key-values pair"""


class RedisStorage:
    def __init__(self, conn: redis.Redis) -> None:
        self.conn = conn

    def get_value(self, key: str) -> Any | None:
        return self.conn.get(name=key)

    def set_value(self, key: str, value: Any) -> None:
        self.conn.set(name=key, value=value)


class MockStorage:
    def __init__(self) -> None:
        self.store = {}

    def get_value(self, key: str) -> Any | None:
        return self.store.get(key, None)

    def set_value(self, key: str, value: Any) -> None:
        self.store[key] = value
