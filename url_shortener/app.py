"""
An URL-Shortener based on Flask, hashids and Redis.
"""

import hashlib
import os

import flask
import validators

from url_shortener.storage import MockStorage, Storage

# Load environment variables
FLASK_SECRET_KEY = os.getenv("FLASK_SECRET_KEY")
HASHIDS_SALT = os.getenv("HASHIDS_SALT")

STORAGE: Storage = MockStorage()

# base flask configurations
app = flask.Flask(__name__)
app.config["SECRET_KEY"] = FLASK_SECRET_KEY


@app.get("/")
def get_index():
    return flask.render_template("index.html.j2")


@app.post("/")
def post_index():
    url = flask.request.form.get("url", "")
    if validators.url(url):  # type: ignore
        # set the short_id to the first 6 characters of the sha1 of the provided URL
        short_id = hashlib.sha1(url.encode()).hexdigest()[:6]
        STORAGE.set_value(key=short_id, value=url)
        short_url = flask.request.host_url + short_id
    else:
        flask.flash("Valid URL is required! e.g. https://www.example.com")
        short_url = None
    return flask.render_template("index.html.j2", short_url=short_url)


@app.get("/<short_id>")
def url_redirect(short_id: str):
    url = STORAGE.get_value(key=short_id)
    if not url:
        flask.flash("ID not found!")
        flask.abort(404)
    return flask.redirect(location=url)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
