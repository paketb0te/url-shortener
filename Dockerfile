FROM python:slim
COPY ./requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
COPY ./url_shortener ./url_shortener
WORKDIR /url_shortener
EXPOSE 5000
CMD [ "python", "app.py"]